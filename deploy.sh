#!/bin/sh
mv -f -v "$HOME/.vimrc" "$HOME/.vimrc.bak"
ln -s -v "$(pwd)/files/vimrc" "$HOME/.vimrc"
