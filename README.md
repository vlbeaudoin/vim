# vlbeaudoin/vim

Installer par symlink la config vim.

## Repo

[vlbeaudoin/vim](https://git.agecem.com/vlbeaudoin/vim)

## Procédure

### /bin/sh

Installer config dans ~/.vimrc

```
$ ./deploy.sh
```

Installer plugins dans ~/.vim/pack/plugins/start/*

```
$ ./plugins.sh
```
