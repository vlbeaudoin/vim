#!/bin/sh
# Install plugins in the appropriate directory.
# Requires vim8+

plugins=''

# Uncomment any of these lines for plugins you want
#plugins="${plugins} https://github.com/w0rp/ale.git"
#plugins="${plugins} https://github.com/vimwiki/vimwiki.git"
#plugins="${plugins} https://github.com/fatih/vim-go"

pluginsDir="$HOME/.vim/pack/plugins/start/"

echo "Installing Plugins: $plugins"

mkdir -p -v "$pluginsDir" && \
cd "$pluginsDir" && \
for plugin in $plugins; do
  git clone "$plugin" 
done;
